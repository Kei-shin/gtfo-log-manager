import gzip
import platform
import subprocess
import sys
import tkinter as tk
import webbrowser
from datetime import datetime
from os import listdir
from os.path import abspath, dirname, isfile, join, expandvars, getmtime
from zipfile import ZipFile, ZIP_DEFLATED

from PIL import ImageTk, Image
from tkcalendar import DateEntry

from web_api import WebAPI

# Used when compiled through Pyinstaller
import babel.numbers

__version__ = "2.1.1"


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.master = master
        self.init_window()
        self.w_text = None
        self.date_search = ""
        self.log_files = None
        self.files_api = WebAPI()
        self.default_logs_location = expandvars(
            r"%userprofile%\appdata\LocalLow\10 Chambers Collective\GTFO"
        )

    # Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title(f"GTFO Log Manager v{__version__}")
        # allowing the widget to take the full space of the root window
        self.pack(fill=tk.BOTH, expand=1)

        # Get location of the image files
        # First tries to use the _MEIPASS for the PyInstaller onefile method
        # otherwise uses the location of the current script file
        if hasattr(sys, "_MEIPASS"):
            curdir = sys._MEIPASS
        else:
            curdir = join(abspath(dirname(__file__)))
        # Add the 10CC logo to the top left
        img_location = join(curdir, "media", "10Chambers_Logo_h200px.jpg")
        img = ImageTk.PhotoImage(Image.open(img_location))
        tencc_logo = tk.Label(root, image=img)
        tencc_logo.image = img
        tencc_logo.place(x=0, y=0)
        # Add the GTFO logo after
        img_location = join(curdir, "media", "GTFO_Logo_Black_h200px.jpg")
        img = ImageTk.PhotoImage(Image.open(img_location))
        gtfo_logo = tk.Label(root, image=img)
        gtfo_logo.image = img
        gtfo_logo.place(x=215, y=0)

        # Create a search files button
        btn_search_logs = tk.Button(
            root, text="Search For Logs", state=tk.NORMAL, command=self.btn_search_files
        )
        btn_search_logs.place(x=2, y=210)

        # Create a send logs button
        btn_send_logs = tk.Button(
            root, text="Send Logs", state=tk.NORMAL, command=self.send_files
        )
        btn_send_logs.place(x=120, y=210)

        # Create a Report Bug button
        btn_report_bug = tk.Button(
            root, text="Report a Bug", state=tk.NORMAL, command=self.report_bug
        )
        btn_report_bug.place(x=210, y=210)

        # Create a Open Logs Folder button
        btn_open_logs_folder = tk.Button(
            root,
            text="Open Logs Folder",
            state=tk.NORMAL,
            command=self.btn_open_logs_folder,
        )
        btn_open_logs_folder.place(x=310, y=210)

        # Create Menu Object
        menubar = tk.Menu(self.master)
        self.master.config(menu=menubar)
        file = tk.Menu(menubar, tearoff=False)
        file.add_command(label="Search for Files", command=self.btn_search_files)
        file.add_command(label="Open Logs Folder", command=self.btn_open_logs_folder)
        file.add_command(label="Exit", command=self.client_exit)
        menubar.add_cascade(label="File", menu=file)
        # Create a Help/About menu
        help_menu = tk.Menu(menubar, tearoff=False)
        help_menu.add_command(label="About", command=self.btn_help_about)
        menubar.add_cascade(label="Help", menu=help_menu)
        root.update_idletasks()

    def btn_open_logs_folder(self):
        subprocess.Popen(f'explorer "{self.default_logs_location}"')

    def report_bug(self):
        # Opens their browser to the Jira page for submitting a bug
        webbrowser.open(
            "https://10chambers.atlassian.net/secure/CreateIssue.jspa?issuetype=10006&pid=10001"
        )

    def btn_help_about(self):
        text = """GTFO Log Manager is a tool used to find and send logs to D0cR3d's server for the developers at 10 Chambers.

Maintainers: 
D0cR3d#0001 (82942340309716992)
Kei Shin#4780 (194616519785840640)

Discord: https://discord.gg/GTFO

Source Code: https://bitbucket.org/layer7solutions/gtfo-log-manager/src
"""

        popup = tk.Toplevel()
        popup.title("About This Application")
        w_text = tk.Text(popup)
        w_text.insert(1.0, text)
        w_text.pack(expand=True, fill="both")
        w_text.configure(state="disabled")
        popup.mainloop()

    def client_exit(self):
        exit()

    def btn_search_files(self):
        # Now that we have a menu, should start looking for the files
        self.date_search = self.get_date_to_search()
        self.log_files = self.search_log_files()
        # Now display the files we got
        self.display_files_found()

    def display_files_found(self):
        # Destroy any previous label
        if self.w_text:
            self.w_text.destroy()
        if self.log_files:
            tmp_log_full_path = [
                f"- {self.default_logs_location}\\{filename}"
                for filename in self.log_files
            ]
            file_text = "\n".join(tmp_log_full_path)
            text_label = f"The following log files were found for the date: {self.date_search}.\n\nClick 'Send Logs' if these are the correct log files.\n\n{file_text}"
        else:
            text_label = (
                f"Sorry, unable to find any files for the date: {self.date_search}"
            )
        self.w_text = tk.Text(root, width=70, height=15)
        self.w_text.insert(1.0, text_label)
        self.w_text.place(x=0, y=240)
        # self.w_text.pack(expand=True, fill=tk.X)
        self.w_text.configure(state="disabled")

    def send_files(self):
        if not self.log_files:
            return
        zip_filepath, zip_filename = self.zip_files()
        if zip_filepath and zip_filename:
            # Now let's upload the file
            response = self.files_api.make_request(zip_filepath, zip_filename)
            if response.status_code == 200:
                # Success! File upload. Now provide the file URL to them.
                file_link = response.json()["uri"]
                text = f"The log files were successfully uploaded. Please provide the following link to the person helping you, or include in the bug report you are submitting.\n\n{file_link}"
            elif response.status_code == 413:
                text = (
                    f"There was an error uploading the file. Error: {response.reason}"
                )
            elif response.status_code == 400:
                error = response.json()["error"]
                text = f"There was an error uploading the file. Error: {error}"
            else:
                text = f"There was an error uploading the file. Unknown Error."
        else:
            text = f"There was an error zipping the file. File not uploaded."

        popup = tk.Toplevel()
        popup.title("Log File Status")
        w_text = tk.Text(popup)
        w_text.insert(1.0, text)
        w_text.pack(expand=True, fill="both")
        w_text.configure(state="disabled")
        popup.mainloop()

        # After we're done sending the files going to reset the log_files list to null
        self.log_files = None

    def zip_files(self):
        try:
            hostname = platform.node()
            zip_filename = f"{hostname}.{self.date_search}.zip"
            gzip_filename = f"{zip_filename}.gz"
            temp_folder = expandvars(r"%TEMP%")
            zip_file_path = f"{temp_folder}\\{zip_filename}"
            gzip_file_path = f"{temp_folder}\\{zip_filename}.gz"
            with ZipFile(
                zip_file_path, "w", compression=ZIP_DEFLATED, compresslevel=9
            ) as zip_file:
                for file in self.log_files:
                    # Add multiple files to the zip
                    zip_file.write(f"{self.default_logs_location}\\{file}", file)
            # Now that we have it .zip'd we're going to gzip it to get even smaller
            with open(zip_file_path, "rb") as file_data:
                zip_file_data = file_data.read()
                with gzip.GzipFile(
                    filename=gzip_file_path, mode="wb", compresslevel=9
                ) as gz_file:
                    gz_file.write(zip_file_data)
            # Return the path and file name of the zipped file so we can upload it
            return temp_folder, gzip_filename
        except Exception as err:
            return None, None

    def get_date_to_search(self):
        topLevel = tk.Toplevel(root)
        tk.Label(topLevel, text="Input a date(MM/DD/YYYY) to search for:").pack(
            padx=100, pady=20
        )
        # Setup the input so that we can either type out the date or use the calendar
        cal = DateEntry(topLevel, width=16, borderwidth=2)
        cal.pack(padx=100, pady=20)
        # Making sure the button does button things
        # like making sure we don't return a value until the user says so
        date_is_set = tk.StringVar()
        date_is_set.set("False")
        btnSearch = tk.Button(
            topLevel, text="Search!", command=lambda: date_is_set.set("True")
        )
        btnSearch.pack(padx=100, pady=20)
        # Waiting for the use to click search so we know what date to return
        btnSearch.wait_variable(date_is_set)
        date_search = str(cal.get_date())
        date_search = date_search.replace("-", ".")
        topLevel.destroy()
        return date_search

    def search_log_files(self):
        log_files = [
            file
            for file in listdir(self.default_logs_location)
            if (
                # Is a file (not a directory)
                isfile(join(self.default_logs_location, file))
                # Now we set limits on what files we actually want
                and (
                    (
                        # File ends with CLIENT or MASTER
                        file.endswith("_CLIENT.json")
                        or file.endswith("_MASTER.json")
                        or file.endswith("_CLIENT.txt")
                        or file.endswith("_MASTER.txt")
                        or file.endswith("_NETSTATUS.txt")
                    )
                    # and the date is in the file now
                    and self.date_search in file
                )
                # If the file is either an output_log or player.log
                or file.lower() == "output_log.txt"
                or file.lower() == "player.log"
                or file.lower() == "player-prev.log"
                # Or the file is the GTFO_Settings
                or file.lower() == "gtfo_settings.txt"
            )
        ]
        # Now let's make our non client/master logs both exist and are from the date we are looking for
        try:
            non_client_or_master_log = [
                "output_log.txt",
                "Player-prev.log",
                "Player.log",
            ]
            for file_to_check in non_client_or_master_log:
                output_log_creation = getmtime(
                    join(self.default_logs_location, file_to_check)
                )
                output_log_creation_friendly = datetime.fromtimestamp(
                    output_log_creation
                ).strftime("%Y.%m.%d")
                if self.date_search != output_log_creation_friendly:
                    log_files.remove(file_to_check)
        except FileNotFoundError:
            pass
        return log_files


if __name__ == "__main__":
    root = tk.Tk()
    # size of the window
    root.geometry("575x500")

    app = Application(root)
    root.mainloop()
